# libstructs

library with structures for general use

## make
library is made and header are copied to include directory

copy include and lib directory to project

## linking
`-I includes -L lib -lstructs`

## depencencies
```mermaid
flowchart TD
_dyn_array.h_[dyn_array.h]
_sort.h_[sort.h]
_string.c_[string.c]
_bin_tree.c_[bin_tree.c]
_dyn_array.c_[dyn_array.c]
_stdlib.h_[stdlib.h]
_math.h_[math.h]
_dyn_matrix.h_[dyn_matrix.h]
_stdio.h_[stdio.h]
_structures.h_[structures.h]
_sort.c_[sort.c]
_dyn_matrix.c_[dyn_matrix.c]
_string.h_[string.h]
_stdbool.h_[stdbool.h]
_list_sort.c_[list_sort.c]
_list_sort.h_[list_sort.h]
_list.h_[list.h]
_list.c_[list.c]
_bin_tree.h_[bin_tree.h]
_stdio.h_ ---> _bin_tree.h_
_stdio.h_ ---> _string.h_
_list.h_ --> _list.c_
_bin_tree.h_ --> _structures.h_
_stdlib.h_ ---> _sort.h_
_stdlib.h_ ---> _list.h_
_dyn_array.h_ --> _structures.h_
_stdbool.h_ ---> _string.h_
_stdlib.h_ ---> _bin_tree.h_
_stdlib.h_ ---> _string.h_
_stdio.h_ ---> _dyn_matrix.h_
_dyn_array.h_ --> _string.h_
_stdlib.h_ ---> _dyn_matrix.h_
_list.h_ --> _list_sort.h_
_sort.h_ --> _list_sort.h_
_bin_tree.h_ --> _bin_tree.c_
_list_sort.h_ --> _list_sort.c_
_sort.h_ --> _sort.c_
_string.h_ --> _structures.h_
_dyn_matrix.h_ --> _dyn_matrix.c_
_list_sort.h_ --> _structures.h_
_string.h_ --> _sort.h_
_dyn_matrix.h_ --> _structures.h_
_list.h_ --> _structures.h_
_sort.h_ --> _structures.h_
_dyn_array.h_ --> _dyn_array.c_
_math.h_ ---> _dyn_matrix.h_
_string.h_ --> _string.c_
_stdlib.h_ ---> _dyn_array.h_
_stdio.h_ ---> _sort.h_
style _math.h_ stroke-dasharray: 5 5
style _stdlib.h_ stroke-dasharray: 5 5
style _stdio.h_ stroke-dasharray: 5 5
style _stdbool.h_ stroke-dasharray: 5 5
```
